import { red } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#556cd6',
      contrastText: '#fff'
    },
    secondary: {
      main: '#19857b',
      contrastText: '#000'
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#282c34',
    },
    text: {
      primary: '#fff',
      secondary: '#000',
      disabled: '#fff',
      hint: '#000',
    },
  },
});

export default theme;
