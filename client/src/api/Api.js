import axios from 'axios';

const API_URL = window.location.hostname === 'localhost' ? 'http://localhost:3333' : 'https://api-place-to-travel.herokuapp.com';

const urls = {
  login: `${API_URL}/api/auth/login`,
  register: `${API_URL}/api/auth/register`,
  currentUser: `${API_URL}/api/auth/current_user`,

  places: `${API_URL}/api/places`,
  myPlaces: `${API_URL}/api/places/my`,

  friends: `${API_URL}/api/friends`,
  addFriend: `${API_URL}/api/friends/request`,

  comments: `${API_URL}/api/comments`,

  trips: `${API_URL}/api/trips`,
};

export const Auth = {
  _token: null,

  get isLoggedIn() {
    return !!this._token;
  },

  setToken(token) {
    this._token = token;

    this._storeToken(token);

    this._setTokenToAxios(token);
  },

  init() {
    try {
      const token = window.localStorage.getItem('token');
      this._token = JSON.parse(token);

      this._setTokenToAxios(this._token);
    } catch (err) {
      return err;
    }
  },

  login({
    username,
    password
  }) {
      return axios.post(urls.login, {
        username,
        password
      });
  },

  logout() {
    this._token = null;
    try {
      window.localStorage.removeItem('token');
    } catch (err) {
      return err;
    }
  },

  register({
    username,
    password
  }) {
    return axios.post(urls.register, {
      username,
      password
    });
  },

  currentUser() {
    return axios.get(urls.currentUser);
  },

  _storeToken() {
    try {
      window.localStorage.setItem('token', JSON.stringify(this._token));
    } catch (error) {
      return error;
    }
  },

  _setTokenToAxios(token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  }
};

export const Place = {
  load() {
    return axios.get(urls.places);
  },
  getMy() {
    return axios.get(urls.myPlaces);
  },
  create(place) {
    return axios.post(urls.places, { ...place });
  }
};

export const Friends = {
  list() {
    return axios.get(urls.friends);
  },
  add(username) {
    return axios.post(urls.addFriend, { friend: username });
  }
};

export const Comments = {
  send({ placeId, text }) {
    return axios.post(`${urls.comments}/${placeId}`, { text });
  },
  get(placeId) {
    return axios.get(`${urls.comments}/${placeId}`);
  }
};

export const Trips = {
  send({ places, title, description, duration, date }) {
    return axios.post(urls.trips, { places, title, description, duration, date });
  },
  getOne(tripId) {
    return axios.get(`${urls.trips}/${tripId}`);
  },
  get() {
    return axios.get(urls.trips);
  },
};
