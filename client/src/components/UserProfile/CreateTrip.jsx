import { Button, Checkbox, Container, List, ListItem, ListItemSecondaryAction, ListItemText, makeStyles, TextField, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../../api';
import Header from '../Header';

const CreateTrip = ({ history }) => {
  const [places, setPlaces] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [checked, setChecked] = React.useState([]);
  const { control, handleSubmit } = useForm();
  const { isLoggedIn } = Api.Auth;

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const onSubmit = async (data) => {
    setLoading(true);
    try {
      await Api.Trips.send({ ...data, places: checked });
      history.push('/trips');
    } catch (error) {
      toast.error(error.response?.data?.message || 'Error while creating trip');
    }
    setLoading(false);
  };

  useEffect(() => {
    const getPlaces = async () => {
      setLoading(true);
      const res = await Api.Place.getMy();
      setPlaces(res.data);
      setLoading(false);
    };
    if (isLoggedIn) {
      getPlaces();
    }
  }, [isLoggedIn]);

  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  }

  const useStyles = makeStyles((theme) => ({
    ListItemText: {
      '& p': { color: theme.palette.text.primary }
    },
    checkbox: {
      color: theme.palette.text.primary
    },
    form: {
      color: theme.palette.text.primary,
      display: 'flex',
      flexDirection: 'column',

      '& input, & textarea, & label': {
        color: theme.palette.text.primary,
      },
    },
    input: {
      margin: '10px 0'
    },
  }));

  const classes = useStyles();

  return (
    <>
      <Header />
      <Container>
        <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
          <Typography variant="body1">Select places:</Typography>
          <List aria-label="places">
            {!isLoading ? places.length ? places
        .sort((a, b) => a.date && b.date ? Date.parse(a.date) - Date.parse(b.date) : -1)
        .map((place) => (
          <ListItem key={place._id}>
            <ListItemText
              primary={place.title}
              secondary={place.date ? new Date(place.date).toLocaleDateString() : null}
              className={classes.ListItemText}
            />
            <ListItemSecondaryAction>
              <Checkbox
                edge="end"
                onChange={handleToggle(place._id)}
                checked={checked.indexOf(place._id) !== -1}
                inputProps={{ 'aria-labelledby': `checkbox-list-secondary-label-${place._id}` }}
                className={classes.checkbox}
              />
            </ListItemSecondaryAction>
          </ListItem>
        )) : null
        : null}
          </List>
          <Controller className={classes.input} control={control} label="Title" name="title" variant="outlined" defaultValue="" required as={TextField} />
          <Controller className={classes.input} control={control} label="Description" name="description" variant="outlined" defaultValue="" as={TextField} />
          <Controller
            label="Visit Date"
            name="date"
            type="date"
            variant="outlined"
            defaultValue=""
            control={control}
            required
            InputLabelProps={{
                shrink: true,
              }}
            as={TextField}
          />
          <Controller className={classes.input} label="Duration" name="duration" variant="outlined" defaultValue="" control={control} as={TextField} />
          <Button variant="contained" color="primary" disabled={isLoading} type="submit">
            {isLoading ? 'Loading...' : 'Create Travel'}
          </Button>
        </form>
      </Container>
    </>
  );
};

export default CreateTrip;
