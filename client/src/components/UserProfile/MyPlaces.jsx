import { Container, List, ListItem, ListItemText } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import Api from '../../api';
import Header from '../Header';

const MyPlaces = () => {
  const [places, setPlaces] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { isLoggedIn } = Api.Auth;

  useEffect(() => {
    const getPlaces = async () => {
      setIsLoading(true);
      const res = await Api.Place.getMy();
      setPlaces(res.data);
      setIsLoading(false);
    };
    if (isLoggedIn) {
      getPlaces();
    }
  }, [isLoggedIn]);

  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  }

  return (
    <>
      <Header />
      <Container>
        <List aria-label="places">
          {!isLoading ? places.length ? places
            .sort((a, b) => a.date && b.date ? Date.parse(a.date) - Date.parse(b.date) : -1)
            .map((place) => (
              <ListItem key={place._id}>
                <ListItemText
                  primary={place.title}
                  secondary={place.date ? new Date(place.date).toLocaleDateString() : null}
                />
              </ListItem>
        )) : <div>You have no places created</div> : null}
        </List>
      </Container>
    </>
  );
};

export default MyPlaces;
