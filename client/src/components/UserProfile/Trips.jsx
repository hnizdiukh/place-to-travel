import { Container, List, ListItem, ListItemText, Link as LinkMUI, makeStyles } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../../api';
import Header from '../Header';

const Trips = () => {
  const [trips, setTrips] = useState([]);
  const [places, setPlaces] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const loadTrips = async () => {
    setLoading(true);
    try {
      const res = await Api.Trips.get();
      setTrips(res.data);
    } catch (error) {
      toast.error(error.response?.data.message || 'Can not load trips');
    }
    setLoading(false);
  };

  const loadPlaces = async () => {
    setLoading(true);
    try {
      const res = await Api.Place.getMy();
      setPlaces(res.data);
    } catch (error) {
      toast.error(error.response?.data.message || 'Can not load places');
    }
    setLoading(false);
  };

  useEffect(() => {
    loadPlaces();
    loadTrips();
  }, []);

  const useStyles = makeStyles((theme) => ({
    ListItemText: {
      '& p': { color: theme.palette.text.primary }
    }
  }));

  const classes = useStyles();

  return (
    <>
      <Header />
      <Container>
        {places.length >= 2
          ? (
            <>
              {trips.length ? (
                <List aria-label="trips">
                  {!isLoading && trips.length ? trips
                    .map((trip) => (
                      <ListItem key={trip._id}>
                        <ListItemText
                          primary={(
                            <div>
                              <p>{trip.title}</p>
                              <p>{trip.description}</p>
                              <p>{trip.date ? new Date(trip.date).toLocaleDateString() : null}</p>
                              <p>{trip.duratoion}</p>
                            </div>
                          )}
                          secondary={(
                            <div><b>Places:</b>
                              {trip.places.map((place) =>
                                <p>{places.find((p) => p._id === place).title}</p>
                              )}
                            </div>
                          )}
                          className={classes.ListItemText}
                        />
                      </ListItem>
                    )) : null}
                </List>
              ) : <div>You have no trips created</div>}
              <LinkMUI color="primary" to="/create-trip" component={Link}>
                Create new trip
              </LinkMUI>
            </>
          )
          : <div>You need to create at least 2 places on the map to create a trip.</div>}
      </Container>
    </>
  );
};

export default Trips;
