import { Button, Container, List, ListItem, ListItemText, Typography, Link as LinkMUI } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../../api';
import Header from '../Header';

const FriendList = () => {
  const [friends, setFriends] = useState([]);
  const [requestOut, setRequestOut] = useState([]);
  const [requestIn, setRequestIn] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { isLoggedIn } = Api.Auth;

  const getFriends = async () => {
    setIsLoading(true);
    const res = await Api.Friends.list();
    setFriends(res.data ? res.data.friends : []);
    setRequestOut(res.data ? res.data.requestOut : []);
    setRequestIn(res.data ? res.data.requestIn : []);
    setIsLoading(false);
  };

  useEffect(() => {
    if (isLoggedIn) {
      getFriends();
    }
  }, [isLoggedIn]);

  const handleAcceptFriend = async (friend) => {
    try {
      await Api.Friends.add(friend);
      getFriends();
    } catch (error) {
      toast.error(error.response?.data.message || 'Can not add friend');
    }
  };

  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  }

return (
  <>
    <Header />
    <Container>

      {(!friends.length && !requestIn.length && !requestOut.length && !isLoading)
    && 'You have no added friends yet.'}

      {friends.length
         ? (
           <List aria-label="friends">
             <Typography variant="body1">Friends:</Typography>
             {friends.map((friend) => (
               <ListItem key={friend}>
                 <ListItemText
                   primary={
                     <LinkMUI component={Link} to={`/user/${friend}`}>{friend}</LinkMUI>
                  }
                 />
               </ListItem>
             ))}
           </List>
         )
         : null}
      {requestIn.length
          ? (
            <List aria-label="friend invintations">
              <Typography variant="body1">Friend invintations:</Typography>
              {requestIn.map((friend) => (
                <ListItem key={friend}>
                  <ListItemText
                    primary={
                      <LinkMUI component={Link} to={`/user/${friend}`}>{friend}</LinkMUI>
                    }
                    secondary={<Button color="primary" onClick={() => handleAcceptFriend(friend)}>Accept</Button>}
                  />
                </ListItem>
              ))}
            </List>
          )
          : null}
      {requestOut.length
           ? (
             <List aria-label="your outcoming friend requests">
               <Typography variant="body1">Your outcoming friend requests:</Typography>
               {requestOut.map((friend) => (
                 <ListItem key={friend}>
                   <ListItemText
                     primary={
                       <LinkMUI component={Link} to={`/user/${friend}`}>{friend}</LinkMUI>
                    }
                   />
                 </ListItem>
               ))}
             </List>
           )
           : null}
    </Container>
  </>
  );
};

export default FriendList;
