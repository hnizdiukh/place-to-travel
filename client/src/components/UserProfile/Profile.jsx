import { Button, Container, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import Api from '../../api';
import Header from '../Header';

const Profile = ({ match }) => {
  const { isLoggedIn } = Api.Auth;
  const [username, setUsername] = useState(null);
  const [friends, setFriends] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const getFriendList = async () => {
    setIsLoading(true);
    try {
      const res = await Api.Friends.list();
      setFriends(res.data);
    } catch (error) {
      toast.error(error.response?.data?.message || 'Can not load friends list');
    }
    setIsLoading(false);
  };

  useEffect(() => {
    getFriendList();
  }, []);

  useEffect(() => {
    async function loadCurrentUser() {
      setIsLoading(true);
      const res = await Api.Auth.currentUser();
      setUsername(res.data.username);
      setIsLoading(false);
    }

    if (isLoggedIn) {
      try {
        loadCurrentUser();
      } catch (error) {
        toast.error(error.response.data.message || 'Can not find current user session');
      }
    }
  }, [isLoggedIn]);

  const handleAddToFriends = async () => {
    try {
      await Api.Friends.add(match.params.username);
      getFriendList();
    } catch (error) {
      toast.error(error.response?.data.message || 'Can not add this person to friends');
    }
  };

  return (
    <div>
      <Header />
      <Container>
        <Typography variant="h5">{match.params.username}</Typography>
        {match.params.username !== username && !isLoading
          ? friends?.friends.includes(match.params.username)
            ? 'Your friend'
            : !friends?.requestOut.includes(match.params.username)
              ? <Button variant="contained" color="primary" onClick={handleAddToFriends}>Add to friends</Button>
              : 'Friendship requested'
          : null}
      </Container>
    </div>
  );
};

export default Profile;
