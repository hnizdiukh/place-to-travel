import { Button, makeStyles, TextField } from '@material-ui/core';
import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../../api';
import LoginHeader from '../LoginHeader';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await Api.Auth.login({ username, password });
      Api.Auth.setToken(res.data.token);
      setUsername('');
      setPassword('');
      history.push('/');
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      minHeight: '100vh',
      flexDirection: 'column'
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '10px',
      '& > input, & label': {
        color: theme.palette.text.primary
      }
    }
  }));

  const classes = useStyles();

  return (
    <div>
      <LoginHeader />
      <div className={classes.root}>
        <form className={classes.form} onSubmit={handleSubmit}>
          <TextField label="Username" variant="outlined" onChange={(e) => setUsername(e.target.value)} required />
          <TextField label="Password" variant="outlined" onChange={(e) => setPassword(e.target.value)} required type="password" />
          <Button variant="contained" color="primary" type="submit">
            Login
          </Button>
        </form>
        Do not have an account?
        <Link to="/register">
          <Button variant="contained" color="secondary">
            Register
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Login;
