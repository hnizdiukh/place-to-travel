import { Button, makeStyles, TextField } from '@material-ui/core';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Joi from 'joi';
import Api from '../../api';
import LoginHeader from '../LoginHeader';

const Register = ({ history }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const schema = Joi.object({
    username: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
    password: Joi.string()
      .min(4)
      .required(),
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await schema.validateAsync({ username, password });
      const res = await Api.Auth.register({ username, password });
      Api.Auth.setToken(res.data.token);
      setUsername('');
      setPassword('');
      history.push('/');
    } catch (error) {
      toast.error(error.response?.data?.message || error.message || 'Wrong credentials');
    }
  };

  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      minHeight: '100vh',
      flexDirection: 'column'
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '10px',
      '& > input, & label': {
        color: theme.palette.text.primary
      }
    },
  }));

  const classes = useStyles();

  return (
    <div>
      <LoginHeader />
      <div className={classes.root}>
        <form className={classes.form} onSubmit={handleSubmit}>
          <TextField label="Username" variant="outlined" onChange={(e) => setUsername(e.target.value)} required />
          <TextField label="Password" variant="outlined" onChange={(e) => setPassword(e.target.value)} required type="password" />
          <Button variant="contained" color="primary" type="submit">
            Register
          </Button>
        </form>
        Already have an account?
        <Link to="/login">
          <Button variant="contained" color="secondary">
            Login
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Register;
