import Header from './Header';
import Map from './Map/Map';

const Home = () => (
  <div>
    <Header />
    <Map />
  </div>
  );

export default Home;
