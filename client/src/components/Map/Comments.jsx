import { Button, List, ListItem, ListItemText, makeStyles, TextField, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../../api';

const Comments = ({ placeId }) => {
  const [comment, setComment] = useState('');
  const [comments, setComments] = useState([]);
  const [isComentsShown, setShowComments] = useState(false);

  useEffect(() => {
    const loadComments = async () => {
      const res = await Api.Comments.get(placeId);
      setComments(res.data);
    };
    loadComments();
  }, [placeId]);

  const loadComments = async () => {
    await Api.Comments.get(placeId);
  };

  const handleCommentSubmit = async (e) => {
    e.preventDefault();
    try {
      await Api.Comments.send({ placeId, text: comment });
      setComment('');
      loadComments();
    } catch (error) {
      toast.error(error.response?.data.message || error.message);
    }
  };

  const useStyles = makeStyles((theme) => ({
    comment: {
      color: theme.palette.text.secondary,
      '& input, & label': {
        color: theme.palette.text.secondary
      },
      display: 'flex',
      flexDirection: 'column',
      marginTop: '10px'
    },
    ListItem: {
      padding: '0 10px'
    }
  }));

  const classes = useStyles();

  return (
    <>
      {comments.length ? (
        <>
          <Button color="secondary" onClick={() => setShowComments(!isComentsShown)}>{isComentsShown ? 'Hide comments' : 'Show comments'}</Button>
          {isComentsShown ? (
            <List>
              {comments.map((c) => (
                <ListItem className={classes.ListItem} key={c._id}>
                  <ListItemText
                    className={classes.comment}
                    primary={(
                      <>
                        <Link to={`user/${c.username}`}>
                          <Typography
                            component="span"
                            variant="body2"
                            color="secondary"
                          >
                            {c.username}
                          </Typography>
                        </Link>
                      </>
                )}
                    secondary={c.text}
                  />
                </ListItem>
          ))}
            </List>
) : null}
        </>
        )
       : null}

      <form
        className={classes.comment}
        onSubmit={handleCommentSubmit}
      >
        <TextField label="Leave comment" variant="outlined" value={comment} onChange={(e) => setComment(e.target.value)} />
      </form>
    </>
  );
};

export default Comments;
