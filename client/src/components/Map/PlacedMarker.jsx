import { makeStyles } from '@material-ui/core';
import { Marker, Popup } from 'react-map-gl';
import { Link } from 'react-router-dom';
import Comments from './Comments';

const PlacedMarker = ({ place, zoom, showPopup, setShowPopup, addPlaceLocation }) => {
  const useStyles = makeStyles((theme) => ({
    marker: {
      '&.red path': {
        fill: '#f05305'
      },

      '&.yellow path': {
        fill: '#F3B73E'
      },
      transform: 'translate(-50%, -100%)'
    },
    popup: {
      color: theme.palette.text.secondary,
      maxWidth: '50vw',
      cursor: 'default'
    },
    index: {
      'z-index': '10'
    }

  }));

  const classes = useStyles();

  return (
    <>
      <Marker
        latitude={place.latitude}
        longitude={place.longitude}
      >
        <div
          onClick={() => setShowPopup({
            [place._id]: true,
          })}
          role="button"
          tabIndex="0"
          onKeyDown={(e) => e.key === 'Enter' && setShowPopup({
            [place._id]: true,
          })}
          style={{ outline: 'none' }}
        >
          <svg
            className={`${classes.marker} yellow`}
            style={{
              height: `${6 * zoom}px`,
              width: `${6 * zoom}px`,
              cursor: 'pointer'
            }}
            version="1.1"
            id="Layer_1"
            x="0px"
            y="0px"
            viewBox="0 0 512 512"
          >
            <g>
              <g>
                <path d="M256,0C153.755,0,70.573,83.182,70.573,185.426c0,126.888,165.939,313.167,173.004,321.035
                        c6.636,7.391,18.222,7.378,24.846,0c7.065-7.868,173.004-194.147,173.004-321.035C441.425,83.182,358.244,0,256,0z M256,278.719
                        c-51.442,0-93.292-41.851-93.292-93.293S204.559,92.134,256,92.134s93.291,41.851,93.291,93.293S307.441,278.719,256,278.719z"
                />
              </g>
            </g>
          </svg>
        </div>
      </Marker>
      {
        showPopup[place._id] && !addPlaceLocation ? (
          <Popup
            latitude={place.latitude}
            longitude={place.longitude}
            closeButton
            closeOnClick={false}
            dynamicPosition
            onClose={() => setShowPopup({})}
            anchor="top"
            className={classes.index}
          >
            <div className={classes.popup}>
              <h3>{place.title}</h3>
              <p>{place.description}</p>
              {place.date
                && (
                  <>
                    <small>Visit on: {new Date(place.date).toLocaleDateString()}</small>
                    <br />
                  </>
                )}
              <small>Duration: {place.duration}</small><br />
              <p>User: <Link to={`/user/${place.username}`}>{place.username}</Link></p>
            </div>
            <Comments placeId={place._id} />
          </Popup>
        ) : null
      }
    </>
  );
};

export default PlacedMarker;
