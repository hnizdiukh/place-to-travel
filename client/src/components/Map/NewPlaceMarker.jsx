import { makeStyles } from '@material-ui/core';
import React from 'react';
import { Marker, Popup } from 'react-map-gl';
import PlaceForm from './PlaceForm';

const NewPlaceMarker = ({ addPlaceLocation, zoom, setAddPlaceLocation, getPlaces }) => {
  const useStyles = makeStyles((theme) => ({
    map: {
      overflow: 'hidden'
    },
    popup: {
      color: theme.palette.text.secondary,
      maxWidth: '50vw'
    },
    marker: {
      '&.red path': {
        fill: '#f05305'
      },
      transform: 'translate(-50%, -100%)'
    },
  }));

  const classes = useStyles();

  return (
    <>
      <Marker
        latitude={addPlaceLocation.latitude}
        longitude={addPlaceLocation.longitude}
      >
        <div>
          <svg
            className={`${classes.marker} red`}
            style={{
              height: `${6 * zoom}px`,
              width: `${6 * zoom}px`,
            }}
            version="1.1"
            id="Layer_1"
            x="0px"
            y="0px"
            viewBox="0 0 512 512"
          >
            <g>
              <g>
                <path d="M256,0C153.755,0,70.573,83.182,70.573,185.426c0,126.888,165.939,313.167,173.004,321.035
                      c6.636,7.391,18.222,7.378,24.846,0c7.065-7.868,173.004-194.147,173.004-321.035C441.425,83.182,358.244,0,256,0z M256,278.719
                      c-51.442,0-93.292-41.851-93.292-93.293S204.559,92.134,256,92.134s93.291,41.851,93.291,93.293S307.441,278.719,256,278.719z"
                />
              </g>
            </g>
          </svg>
        </div>
      </Marker>
      <Popup
        latitude={addPlaceLocation.latitude}
        longitude={addPlaceLocation.longitude}
        closeButton
        closeOnClick={false}
        dynamicPosition
        onClose={() => setAddPlaceLocation(null)}
        anchor="top"
      >
        <div className={classes.popup}>
          <PlaceForm
            onClose={() => {
              setAddPlaceLocation(null);
              getPlaces();
            }}
            location={addPlaceLocation}
          />
        </div>
      </Popup>
    </>
  );
};

export default NewPlaceMarker;
