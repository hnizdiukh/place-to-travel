import { makeStyles } from '@material-ui/core';
import { useEffect, useState } from 'react';
import ReactMapGL from 'react-map-gl';
import Api from '../../api';
import PlacedMarker from './PlacedMarker';
import NewPlaceMarker from './NewPlaceMarker';

const Map = () => {
  const [places, setPlaces] = useState([]);
  const [showPopup, setShowPopup] = useState({});
  const [addPlaceLocation, setAddPlaceLocation] = useState(null);
  const [viewport, setViewport] = useState({
    width: '100vw',
    height: 'calc(100vh - 80px)',
    latitude: 37.6,
    longitude: -95.665,
    zoom: 3
  });

  const getPlaces = async () => {
    const res = await Api.Place.load();
    setPlaces(res.data);
  };

  useEffect(() => {
    getPlaces();
  }, []);

  const showAddMarkerPopup = (event) => {
    const [longitude, latitude] = event.lngLat;
    setAddPlaceLocation({
      latitude,
      longitude,
    });
  };

  const useStyles = makeStyles((theme) => ({
    map: {
      overflow: 'hidden'
    },
    popup: {
      color: theme.palette.text.secondary,
      maxWidth: '50vw'
    },
    marker: {
      '&.red path': {
        fill: '#f05305'
      },
      transform: 'translate(-50%, -100%)'
    },
  }));

  const classes = useStyles();

  return (
    <ReactMapGL
      {...viewport}
      mapStyle="mapbox://styles/thecjreynolds/ck117fnjy0ff61cnsclwimyay"
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
      onViewportChange={setViewport}
      onDblClick={showAddMarkerPopup}
      className={classes.map}
    >
      {
        places.map((place) => (
          <PlacedMarker
            place={place}
            zoom={viewport.zoom}
            showPopup={showPopup}
            setShowPopup={setShowPopup}
            addPlaceLocation={addPlaceLocation}
            key={place._id}
          />
        ))
      }
      {
        addPlaceLocation ? (
          <NewPlaceMarker
            addPlaceLocation={addPlaceLocation}
            zoom={viewport.zoom}
            setAddPlaceLocation={setAddPlaceLocation}
            getPlaces={getPlaces}
          />
        ) : null
      }
    </ReactMapGL>
  );
};

export default Map;
