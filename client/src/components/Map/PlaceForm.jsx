import { Button, makeStyles, TextField } from '@material-ui/core';
import { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../../api';

const PlaceForm = ({ location, onClose }) => {
  const [loading, setLoading] = useState(false);
  const { control, handleSubmit } = useForm();
  const { isLoggedIn } = Api.Auth;

  const onSubmit = async (data) => {
    setLoading(true);
    try {
      await Api.Place.create(
        { ...data, longitude: location.longitude, latitude: location.latitude }
      );
      onClose();
    } catch (error) {
      toast.error(error.response?.data?.message || 'Error while creating travel');
    }
    setLoading(false);
  };

  const useStyles = makeStyles((theme) => ({
    form: {
      color: theme.palette.text.secondary,
      display: 'flex',
      flexDirection: 'column'
    },
    input: {
      '& input, & textarea, & label': {
        color: theme.palette.text.secondary,
      },
      margin: '10px 0'
    },
    guest: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '& button': {
        marginTop: '15px'
      }
    }
  }));

  const classes = useStyles();

  return (
    <>
      {!isLoggedIn
        ? (
          <div className={classes.guest}>
            To create Place you need to be logged in
            <Link to="/login">
              <Button variant="contained" color="primary">
                Login
              </Button>
            </Link>
            <Link to="/register">
              <Button variant="contained" color="secondary">
                Register
              </Button>
            </Link>
          </div>
          )
        : (
          <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
            <Controller className={classes.input} control={control} label="Title" name="title" variant="outlined" defaultValue="" required as={TextField} />
            <Controller className={classes.input} label="Description" name="description" variant="outlined" defaultValue="" control={control} multiline rows={4} as={TextField} />
            <Controller
              className={classes.input}
              label="Visit Date"
              name="date"
              type="date"
              variant="outlined"
              defaultValue=""
              control={control}
              required
              InputLabelProps={{
                shrink: true,
              }}
              as={TextField}
            />
            <Controller className={classes.input} label="Duration" name="duration" variant="outlined" defaultValue="" control={control} as={TextField} />
            <Button variant="contained" color="primary" disabled={loading} type="submit">
              {loading ? 'Loading...' : 'Create Travel'}
            </Button>
          </form>
        )}
    </>
  );
};

export default PlaceForm;
