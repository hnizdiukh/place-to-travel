import { Button, makeStyles, Menu, MenuItem, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import Api from '../api';

const Header = () => {
  const { isLoggedIn } = Api.Auth;
  const history = useHistory();
  const [username, setUsername] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    async function loadCurrentUser() {
      const res = await Api.Auth.currentUser();
      setUsername(res.data.username);
    }

    if (isLoggedIn) {
      try {
        loadCurrentUser();
      } catch (error) {
        toast.error(error.response.data.message || 'Can not find current user session');
      }
    }
  }, [isLoggedIn]);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    Api.Auth.logout();
    history.push('/login');
  };

  const useStyles = makeStyles((theme) => ({
    header: {
      margin: '10px 40px',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    link: {
      textDecoration: 'none',
      color: theme.palette.text.primary
    },
    menuItem: {
      color: 'black',
      '& a': {
        textDecoration: 'none',
        color: theme.palette.text.secondary,
        outline: 'none'
      }
    },
    menu: {
      display: 'flex',
      justifyContent: 'flex-end',
      margin: '10px'
    },
  }));

  const classes = useStyles();

  return (
    <header className={classes.header}>
      <Link to="/" className={classes.link}><Typography variant="h5">🌍 Place to travel</Typography></Link>
      <div className={classes.menu}>
        {isLoggedIn
          ? (
            <>
              <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                {username}
              </Button>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem
                  onClick={handleClose}
                  className={classes.menuItem}
                >
                  <Link to="/friends">My friends
                  </Link>
                </MenuItem>
                <MenuItem
                  onClick={handleClose}
                  className={classes.menuItem}
                >
                  <Link to="/places">My places
                  </Link>
                </MenuItem>
                <MenuItem
                  onClick={handleClose}
                  className={classes.menuItem}
                >
                  <Link to="/trips">Trips
                  </Link>
                </MenuItem>
                <MenuItem
                  onClick={() => { handleClose(); handleLogout(); }}
                  className={classes.menuItem}
                >
                  Logout
                </MenuItem>
              </Menu>
            </>
          )
          : <><Link to="/login"><Button>Login</Button></Link></>}

      </div>
    </header>
  );
};

export default Header;
