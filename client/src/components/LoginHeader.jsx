import { makeStyles, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

const LoginHeader = () => {
  const useStyles = makeStyles((theme) => ({
    header: {
      position: 'absolute',
      top: '20px',
      left: '40px'
    },
    link: {
      textDecoration: 'none',
      color: theme.palette.text.primary
    }
  }));

  const classes = useStyles();

  return (
    <header className={classes.header}><Link to="/" className={classes.link}><Typography variant="h5">🌍 Place to travel</Typography></Link></header>
  );
};

export default LoginHeader;
