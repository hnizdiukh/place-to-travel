import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import theme from './theme';
import Login from './components/Auth/Login';
import Register from './components/Auth/Register';
import Home from './components/Home';
import Api from './api';
import MyPlaces from './components/UserProfile/MyPlaces';
import Profile from './components/UserProfile/Profile';
import FriendList from './components/UserProfile/FriendList';
import Trips from './components/UserProfile/Trips';
import CreateTrip from './components/UserProfile/CreateTrip';

import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  Api.Auth.init();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <ToastContainer />
      <Router>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/places" component={MyPlaces} />
          <Route path="/friends" component={FriendList} />
          <Route path="/trips" component={Trips} />
          <Route path="/create-trip" component={CreateTrip} />
          <Route path="/user/:username" component={Profile} />
          <Route path="/" exact component={Home} />
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

ReactDOM.render(
  <App />,
  document.querySelector('#root'),
);
