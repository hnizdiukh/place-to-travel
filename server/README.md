# PLACE

* _id
* Title
* Planned date for visit
* Duration for being there
* Latitude
* Longitude
* username - FK
* createdAt
* Description

# USER

* Username
* Password
* createdAt

# COMMENT

* text
* username - FK
* createdAt
* updatedAt
* placeId - FK

# TRIP

* name
* places (array)

# FRIEND LIST

* username
* friendsList (array)
* requestIn (array)
* requestOut (array)