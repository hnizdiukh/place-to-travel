const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const placeSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: String,
  date: Date,
  duration: String,
  latitude: {
    type: Number,
    required: true,
    min: -90,
    max: 90
  },
  longitude: {
    type: Number,
    required: true,
    min: -180,
    max: 180
  },
  username: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

const Place = mongoose.model('Place', placeSchema);

module.exports = Place;
