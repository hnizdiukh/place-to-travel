const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const commentSchema = new Schema({
  text: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  placeId: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

const Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;
