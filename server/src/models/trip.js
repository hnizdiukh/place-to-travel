const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const tripSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  places: { type: Array },
  description: String,
  date: Date,
  duration: String,
  username: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

const Trip = mongoose.model('Trip', tripSchema);

module.exports = Trip;
