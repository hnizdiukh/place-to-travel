const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const friendsSchema = new Schema({
  username: {
    type: String,
    required: true
  },
  friends: { type: Array },
  requestIn: { type: Array },
  requestOut: { type: Array }
});

const FriendList = mongoose.model('FriendList', friendsSchema);

module.exports = FriendList;
