const jwt = require('jsonwebtoken');

require('dotenv').config();

const notFound = (req, res, next) => {
  const error = new Error(`Not Found - ${req.originalUrl}`);
  res.status(404);
  next(error);
};

// eslint-disable-next-line no-unused-vars
const errorHandler = (error, req, res, next) => {
  const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
  res.status(statusCode);
  res.json({
    message: error.message,
    stack: process.env.NODE_ENV === 'production' ? '🔒' : error.stack
  });
};

const authMiddleware = async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;

    if (!authorization) {
      res.status(401);
      throw new Error('Not authorized');
    }
    const token = authorization.split(' ')[1];

    if (!token) {
      res.status(401);
      throw new Error('Not authorized');
    }

    if ((await jwt.verify(token, process.env.JWT_KEY || '3cab0ec2386b73e5ca92c4799f9441cbf58820dbedbf8a99'))) {
      await next();
    } else {
      res.status(401);
      throw new Error('Invalid JWT token');
    }
  } catch (error) {
    next(error);
  }
};

module.exports = {
  notFound, errorHandler, authMiddleware
};
