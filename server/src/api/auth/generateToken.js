const jwt = require('jsonwebtoken');

require('dotenv').config();

const key = process.env.JWT_KEY || '3cab0ec2386b73e5ca92c4799f9441cbf58820dbedbf8a99';

const generateToken = (payload) => jwt.sign(payload, key, { algorithm: 'HS256' });

module.exports = generateToken;
