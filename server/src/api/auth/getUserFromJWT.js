const jwt = require('jsonwebtoken');
const UserModel = require('../../models/user');

require('dotenv').config();

const getUserFromJWT = async (token) => {
  try {
    if (!token) {
      return;
    }

    const decoded = await jwt.verify(token, process.env.JWT_KEY || '');

    if (!decoded || !decoded.iss) {
      throw new Error('Invalid JWT');
    }

    const user = await UserModel.findOne({ username: decoded.iss });

    if (!user) {
      return;
    }

    delete user.password;

    return user;
  } catch (error) {
    return null;
  }
};

module.exports = getUserFromJWT;
