const express = require('express');
const bcrypt = require('bcrypt');

const getUserFromJWT = require('./getUserFromJWT');
const generateToken = require('./generateToken');
const UserModel = require('../../models/user');
const { authMiddleware } = require('../../middlewares');

const router = express.Router();

require('dotenv').config();

const login = async (req, res, next) => {
  try {
    const { body } = req;

    const user = body.username
    ? await UserModel.findOne({ username: body.username.toLowerCase() }) : null;

    if (!user) {
      throw new Error('Wrong credentials');
    }

    const passwordMatch = await bcrypt.compareSync(
      body.password,
      user.password || '',
    );

    if (!passwordMatch) {
      throw new Error('Wrong password');
    }

    const token = generateToken({
      iss: user.username,
    });

    if (!token) {
      throw new Error('JWT token is not created');
    }

    res.status(200);
    res.json({ token });
  } catch (error) {
    res.status(401);
    next(error);
  }
};

const register = async (req, res, next) => {
  try {
    const { body } = req;
    const user = {};

    if (!body.username) {
      throw new Error('username is required');
    }

    if (!body.password) {
      throw new Error('password is required');
    }

    const existedUser = await UserModel.findOne({ username: body.username });

    if (existedUser) {
      throw new Error('User with such username is already exist');
    }

    user.username = body.username;
    user.password = await bcrypt.hashSync(body.password, 10);
    user.createdAt = Date.now();

    const token = generateToken({
      iss: user.username,
    });

    if (!token) {
      throw new Error('JWT token is not created');
    }

    const newUser = new UserModel(user);
    await newUser.save();

    const { password, ...userResponse } = user;

    res.status(201);
    res.json({ token, user: userResponse });
  } catch (error) {
    res.status(401);
    next(error);
  }
};

const getCurrentUser = async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;

    if (!authorization) {
      throw new Error('Should be logged in');
    }

    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);

    if (!user) {
      throw new Error('Wrong JWT token');
    }

    const { password, ...userResponse } = user._doc;

    res.status(200);
    res.json(userResponse);
  } catch (error) {
    res.status(401);
    next(error);
  }
};

const getUser = async (req, res, next) => {
  const { id } = req.params;
  try {
    const user = await UserModel.findOne({ id });

    if (!user) {
      throw new Error('No user with such id');
    }

    const { password, ...userResponse } = user;
    res.json(userResponse);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

router.post('/login', login);
router.post('/register', register);
router.get('/current_user', authMiddleware, getCurrentUser);
router.get('/user/:id', authMiddleware, getUser);

module.exports = router;
