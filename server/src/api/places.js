const express = require('express');
const { authMiddleware } = require('../middlewares');

const router = express.Router();

const PlaceModel = require('../models/place');
const getUserFromJWT = require('./auth/getUserFromJWT');

router.get('/', async (req, res, next) => {
  try {
    const places = await PlaceModel.find();
    res.json(places);
  } catch (error) {
    next(error);
  }
});

router.post('/', authMiddleware, async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);
    const place = new PlaceModel({ ...req.body, username: user.username });
    const createdPlace = await place.save();
    res.json(createdPlace);
    res.status(201);
  } catch (error) {
    res.status(422);
    next(error);
  }
});

router.get('/my', authMiddleware, async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);
    const places = await PlaceModel.find({ username: user.username });
    res.json(places);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
