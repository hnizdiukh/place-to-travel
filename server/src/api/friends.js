const express = require('express');
const { authMiddleware } = require('../middlewares');

const router = express.Router();

const FriendsModel = require('../models/friendList');
const UserModel = require('../models/user');
const getUserFromJWT = require('./auth/getUserFromJWT');

router.get('/', async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);
    const friends = await FriendsModel.findOne({ username: user.username });
    res.json(friends);
  } catch (error) {
    next(error);
  }
});

router.post('/request', authMiddleware, async (req, res, next) => {
  try {
    const friendUsername = req.body.friend;

    if (!friendUsername) {
      throw new Error('"friend" is required field');
    }

    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);

    const searchedFriend = await UserModel.findOne({ username: friendUsername });
    if (!searchedFriend) {
      throw new Error('No user with such username');
    }

    const document = await FriendsModel.findOne({ username: user.username });

    if (document && document.friends.includes(friendUsername)) {
      throw new Error('User is already in friends list');
    }

    const queueIn = document ? document.requestIn || [] : [];

    let update;
    let updateFriend;

    if (queueIn.includes(friendUsername)) {
      const friendsDocument = await FriendsModel.findOne({ username: friendUsername });
      const queueOut = friendsDocument ? friendsDocument.requestOut || [] : [];
      const newRequestsOut = queueOut.filter((u) => u !== user.username);
      const newRequestsIn = queueIn.filter((u) => u !== friendUsername);

      update = await FriendsModel.updateOne(
        { username: user.username },
        { $addToSet: { friends: [friendUsername] }, $set: { requestIn: newRequestsIn } },
        { upsert: true }
      );
      updateFriend = await FriendsModel.updateOne(
        { username: friendUsername },
        { $addToSet: { friends: [user.username] }, $set: { requestOut: newRequestsOut } },
        { upsert: true }
      );
    } else {
      update = await FriendsModel.updateOne(
        { username: user.username },
        { $addToSet: { requestOut: [friendUsername] } },
        { upsert: true }
      );
      updateFriend = await FriendsModel.updateOne(
        { username: friendUsername },
        { $addToSet: { requestIn: [user.username] } },
        { upsert: true }
      );
    }

    res.json({
      n: update.n + updateFriend.n,
      nModified: update.nModified + updateFriend.nModified,
      ok: update.ok
    });
    res.status(201);
  } catch (error) {
    res.status(422);
    next(error);
  }
});

module.exports = router;
