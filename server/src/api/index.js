const express = require('express');

const places = require('./places');
const auth = require('./auth/users');
const comments = require('./comments');
const trips = require('./trips');
const friends = require('./friends');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'API - 👋🌎🌍🌏'
  });
});

router.use('/places', places);
router.use('/auth', auth);
router.use('/comments', comments);
router.use('/trips', trips);
router.use('/friends', friends);

module.exports = router;
