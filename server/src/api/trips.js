const express = require('express');
const { authMiddleware } = require('../middlewares');

const router = express.Router();

const TripModel = require('../models/trip');
const getUserFromJWT = require('./auth/getUserFromJWT');

router.get('/', authMiddleware, async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);
    const trips = await TripModel.find({ username: user.username });
    res.json(trips);
  } catch (error) {
    next(error);
  }
});

router.get('/:tripId', async (req, res, next) => {
  try {
    const { tripId } = req.params;
    const trips = await TripModel.find({ _id: tripId });
    res.json(trips);
  } catch (error) {
    next(error);
  }
});

router.post('/', authMiddleware, async (req, res, next) => {
  try {
    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);
    if (!req.body.places || req.body.places.length < 2) {
      throw new Error('Clarify at least 2 places');
    }
    const trip = new TripModel({ ...req.body, username: user.username });
    const createdTrip = await trip.save();
    res.json(createdTrip);
    res.status(201);
  } catch (error) {
    res.status(422);
    next(error);
  }
});

module.exports = router;
