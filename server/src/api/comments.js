const express = require('express');
const { authMiddleware } = require('../middlewares');

const router = express.Router();

const CommentModel = require('../models/comment');
const getUserFromJWT = require('./auth/getUserFromJWT');

router.get('/:placeId', async (req, res, next) => {
  try {
    const { placeId } = req.params;
    const comments = await CommentModel.find({ placeId });
    res.json(comments);
  } catch (error) {
    next(error);
  }
});

router.post('/:placeId', authMiddleware, async (req, res, next) => {
  try {
    const { placeId } = req.params;
    const { headers: { authorization } } = req;
    const token = authorization.split(' ')[1];
    const user = await getUserFromJWT(token);
    const comment = new CommentModel({ ...req.body, placeId, username: user.username });
    const createdComment = await comment.save();
    res.json(createdComment);
    res.status(201);
  } catch (error) {
    res.status(422);
    next(error);
  }
});

module.exports = router;
